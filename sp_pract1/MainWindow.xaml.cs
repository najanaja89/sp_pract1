﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Win32;
using sp_pract1.Models;

namespace sp_pract1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string fromFolder = "";
        private string toFolder = "";
        public MainWindow()
        {
            InitializeComponent();
            var taskPeriod = new List<string>()
            {
                "Once",
                "Daily",
                "Weekly",
                "Month",
                "Year"
            };
            var taskType = new List<string>()
            {
                "Download File",
                "Move Catalog",
                "Send E-Mail"
            };
            var task = new ScheduledTask();

            periodCB.ItemsSource = taskPeriod;
            typeCB.ItemsSource = taskType;
        }

        private void TypeCBSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var select = typeCB.SelectedItem.ToString();
            if (select == "Download File")
            {
                textBoxEmail.Visibility = Visibility.Hidden;
                richTextBoxEmail.Visibility = Visibility.Hidden;

                textBoxDownload.Visibility = Visibility.Visible;
                textBoxDownloadFileName.Visibility = Visibility.Visible;
                progressBarDownLoad.Visibility = Visibility.Hidden;

                buttonFromMove.Visibility = Visibility.Hidden;
                buttonWhereMove.Visibility = Visibility.Hidden;
            }
            else if (select == "Send E-Mail")
            {
                textBoxEmail.Visibility = Visibility.Visible;
                richTextBoxEmail.Visibility = Visibility.Visible;

                textBoxDownload.Visibility = Visibility.Hidden;
                textBoxDownloadFileName.Visibility = Visibility.Hidden;
                progressBarDownLoad.Visibility = Visibility.Hidden;

                buttonFromMove.Visibility = Visibility.Hidden;
                buttonWhereMove.Visibility = Visibility.Hidden;
            }
            else if (select == "Move Catalog")
            {
                textBoxEmail.Visibility = Visibility.Hidden;
                richTextBoxEmail.Visibility = Visibility.Hidden;

                textBoxDownload.Visibility = Visibility.Hidden;
                textBoxDownloadFileName.Visibility = Visibility.Hidden;
                progressBarDownLoad.Visibility = Visibility.Hidden;

                buttonFromMove.Visibility = Visibility.Visible;
                buttonWhereMove.Visibility = Visibility.Visible;
            }
        }

        private void ButtonFromMoveClick(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                dialog.ShowDialog();
                var result = dialog.SelectedPath;
                fromFolder = result;
            }

        }

        private void ButtonWhereMoveClick(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                dialog.ShowDialog();
                var result = dialog.SelectedPath;
                toFolder = result;
            }
        }

        private void SubmitTaskClick(object sender, RoutedEventArgs e)
        {
            var select = typeCB.SelectedItem.ToString();
            if (select == "Download File")
            {
                using (var webClient = new WebClient())
                {
                    string pathSave = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "//" + textBoxDownloadFileName.Text;
                    progressBarDownLoad.Visibility = Visibility.Visible;
                    webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadFileCompleted);
                    webClient.DownloadFileAsync(new Uri (textBoxDownload.Text), pathSave);
                }
            }
            else if (select == "Move Catalog")
            {

                string folderName = new DirectoryInfo(fromFolder).Name;
                string whereFolder = toFolder + "\\" + folderName;
                if (Directory.Exists(whereFolder))
                {
                    Directory.Delete(whereFolder);
                }
                Directory.Move(fromFolder, whereFolder);
            }
            else if (select == "Send E-Mail")
            {
                try
                {
                    MailAddress from = new MailAddress("najanajatest@gmail.com", "Test");
                    MailAddress to = new MailAddress("texa_rus@bk.ru");
                    MailMessage message = new MailMessage(from, to);

                    message.Subject = "Task Email" + DateTime.Now.ToShortDateString();
                    message.Body = new TextRange(richTextBoxEmail.Document.ContentStart, richTextBoxEmail.Document.ContentEnd).Text;
                    message.IsBodyHtml = true;

                    SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                    smtp.Credentials = new NetworkCredential("najanajatest@gmail.com", "Rus89lan");
                    smtp.EnableSsl = true;
                    smtp.Send(message);

                    //var client = new SmtpClient("smtp.gmail.com", 587)
                    //{
                    //    Credentials = new NetworkCredential("najanajatest@gmail.com", "Rus89lan"),
                    //    EnableSsl = true,                 
                    //};
                    //client.Send("najanajatest@gmail.com", "najanaja89@gmail.com", "test", "testbody");
                }

                catch (SmtpException exception)
                {
                    throw new ApplicationException
                      ("SmtpException has occured: " + exception.Message);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        private void DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            progressBarDownLoad.Visibility = Visibility.Hidden;
        }
    }
}
