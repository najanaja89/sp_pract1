﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sp_pract1.Models
{
    public class ScheduledTask
    {
        public Guid GuidId { get; set; } = Guid.NewGuid();
        public DateTime TaskStartTime { get; set; } = DateTime.Now;
        public DateTime TaskEndTime { get; set; }
        public string TaskType { get; set; }
        public string TaskPeriod { get; set; }
        public bool TaskDone { get; set; } = false;

    }
}
